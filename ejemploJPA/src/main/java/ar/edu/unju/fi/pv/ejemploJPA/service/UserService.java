package ar.edu.unju.fi.pv.ejemploJPA.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.pv.ejemploJPA.dao.UserRepository;
import ar.edu.unju.fi.pv.ejemploJPA.model.User;

@Service	
public class UserService {
	@Autowired
	private UserRepository userRepository;
	
	public User getUserByUsername(String username) {
		return userRepository.getUserByUsername(username);	
	}
}
