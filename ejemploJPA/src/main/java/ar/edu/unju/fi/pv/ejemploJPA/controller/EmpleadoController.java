package ar.edu.unju.fi.pv.ejemploJPA.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import ar.edu.unju.fi.pv.ejemploJPA.model.Empleado;
import ar.edu.unju.fi.pv.ejemploJPA.service.DepartamentoService;
import ar.edu.unju.fi.pv.ejemploJPA.service.EmpleadoService;

@Controller
public class EmpleadoController {
	@Autowired
	private EmpleadoService empleadoService;
	@Autowired
	private DepartamentoService departamentoService;
	
	@GetMapping("/")
	public String getListaEmpleados(Model model) {
		model.addAttribute("operacion", "Lista Empleados");
		List<Empleado> empleados = empleadoService.traerTodos();
		model.addAttribute("empleados", empleados);
		return "empleados";
	}
	
	@GetMapping("/altaEmpleado")
	public String getAltaEmpleado(Model model) {
		Empleado empleado = new Empleado();		
		model.addAttribute("operacion", "Alta Empleado");
		model.addAttribute("departamentos", departamentoService.traerTodos());
		model.addAttribute("empleado", empleado);
		return "cargaEmpleado";
	}
	
	@PostMapping("/guardar")
	public String guardarEmpleado(Model model, @ModelAttribute("empleado") Empleado empleado) {
		empleadoService.guardar(empleado);
		return "redirect:/";
	}
  	
}
