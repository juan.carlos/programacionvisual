package ar.edu.unju.fi.pv.ejemploJPA.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.pv.ejemploJPA.model.Departamento;
@Repository
public interface DepartamentoRepository extends JpaRepository<Departamento, Integer>{

}
