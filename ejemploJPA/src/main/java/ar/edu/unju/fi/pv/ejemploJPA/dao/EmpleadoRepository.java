package ar.edu.unju.fi.pv.ejemploJPA.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.pv.ejemploJPA.model.Empleado;

@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, Integer>{
			
	List<Empleado> findByNombre(String nombre);
	
}
