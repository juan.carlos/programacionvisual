package ar.edu.unju.fi.pv.ejemploJPA.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.pv.ejemploJPA.dao.EmpleadoRepository;
import ar.edu.unju.fi.pv.ejemploJPA.model.Empleado;

@Service	
public class EmpleadoService {
	@Autowired
	EmpleadoRepository empleadoRepository;

	public List<Empleado> traerTodos() {
		return empleadoRepository.findAll();
	}
	
	public List<Empleado> traerByNombre(String nombre) {
		return empleadoRepository.findByNombre(nombre);
	}

	public void guardar(Empleado empleado) {
		empleadoRepository.save(empleado);
	}
}	
