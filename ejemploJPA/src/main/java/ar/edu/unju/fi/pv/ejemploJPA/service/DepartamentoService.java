package ar.edu.unju.fi.pv.ejemploJPA.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.pv.ejemploJPA.dao.DepartamentoRepository;
import ar.edu.unju.fi.pv.ejemploJPA.model.Departamento;

@Service
public class DepartamentoService {
	@Autowired
	private DepartamentoRepository departamentoRepository;

	public List<Departamento> traerTodos() {
		return departamentoRepository.findAll();
	}
	
}
