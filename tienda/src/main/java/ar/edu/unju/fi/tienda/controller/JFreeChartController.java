package ar.edu.unju.fi.tienda.controller;

import java.awt.Font;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.servlet.ServletUtilities;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class JFreeChartController {
	@RequestMapping("graficoLineaChart")
	public String graficoLineaChart(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
         // Define chart object data, data
		DefaultCategoryDataset  dataset  =createDataset();
//		JFreeChart  chart = ChartFactory.createLineChart(
//			"Reclamos  - Gráfico de Linar", // chart title
//             "time", // domain axis label
//             "Sales volume(Million)", // range axis label
//             dataset, // data
//			PlotOrientation.VERTICAL, // orientation
//			true, // include legend
//			true, // tooltips
//			false // urls
//				);

		JFreeChart  chart = ChartFactory.createLineChart(
				"Reclamos por Barrio - Gráfico de Lineas", // chart title
	            "Año 2017", // domain axis label
	            "Cantidad", // range axis label
	            dataset, // data
				PlotOrientation.VERTICAL, // orientation
				true, // include legend
				true, // tooltips
				false // urls
				);

		CategoryPlot plot = chart.getCategoryPlot();
		// Set icon font 
		chart.getTitle().setFont(new Font("Song style", Font.BOLD, 22)); 
		//Set font for horizontal axis
		CategoryAxis categoryAxis = plot.getDomainAxis();
		 categoryAxis.setLabelFont(new Font("Song style", Font.BOLD, 12));//x-axis title font 
		 categoryAxis.setTickLabelFont(new Font("Song style", Font.PLAIN, 16));//x-axis scale font
		 
		 //The following two lines set the font for the legend
		LegendTitle legend = chart.getLegend(0);
		legend.setItemFont(new Font("Song style", Font.BOLD, 14));
		 //Set font for vertical axis
		NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setLabelFont(new Font("Song style" , Font.BOLD , 12)); //Set font for axis
		rangeAxis.setTickLabelFont(new Font("Song style" , Font.PLAIN, 16));
		
//		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());//Remove the vertical axis font display incomplete
//		 rangeAxis.setAutoRangeIncludesZero(true);
//		 rangeAxis.setUpperMargin(0.20);
//		 rangeAxis.setLabelAngle(Math.PI / 2.0);
		 
		// 6. Convert graphics to pictures and send them to the front desk
			String fileName = ServletUtilities.saveChartAsJPEG(chart, 700, 400, null, request.getSession());
			String chartURL = request.getContextPath() + "/chart?filename=" + fileName;
			model.addAttribute("makeLineAndShapeChart", chartURL);
			
		 return "viewChart";
	}

	// Generate data
	public static DefaultCategoryDataset createDataset() {
		DefaultCategoryDataset linedataset = new DefaultCategoryDataset();
		 // Name of each curve
		String [] series= {"PALPALA","RIO BLANCO","SAN MARTIN", "LOMA GOLF", "BELGRANO"};
		// Horizontal axis name (column name)
		String [] month = {"Enero","Febrero", "Marzo","Abril"};
		//Specific data
		double [] num = {7,9,15,12};
	 
		//int l1 = num.length/series.length; //4
		int l1 = 1;
		int l2 = month.length;
		int j=0;
		for(int i=0;i<num.length;i++) {
			linedataset.addValue(num[i], series[i/l1], month[j]);	
			j++;
			if(j==month.length)
				j=0;
		}			 
		return linedataset;
	}		


		@RequestMapping("graficoBarraChart")
		public String graficoBarraChart(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
			CategoryDataset dataset = createDataset();
			
			 JFreeChart chart = ChartFactory.createBarChart(
		                "Reclamos por Barrio - Gráfico de Barras",		                
		                "Año 2017",
		                "Cantidad",
		                dataset,
		                PlotOrientation.VERTICAL,
		                true, true, false);
				
				CategoryPlot plot = chart.getCategoryPlot();
				// Set icon font 
				chart.getTitle().setFont(new Font("Song style", Font.BOLD, 22)); 
				//Set font for horizontal axis
							
			 String fileName = ServletUtilities.saveChartAsJPEG(chart, 700, 400, null, request.getSession());
				String chartURL = request.getContextPath() + "/chart?filename=" + fileName;
				model.addAttribute("makeLineAndShapeChart", chartURL);
				
			 return "viewChart";

	        
		}
}
