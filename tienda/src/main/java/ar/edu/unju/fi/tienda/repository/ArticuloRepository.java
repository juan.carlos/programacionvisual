package ar.edu.unju.fi.tienda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unju.fi.tienda.domain.Articulo;
@Repository
public interface ArticuloRepository extends JpaRepository<Articulo, Integer> {
	
	@Query("from Articulo a order by a.nombre")
	public List<Articulo>buscarTodos();
	
	public List<Articulo> findByNombreContaining(String nombre);
	
	@Transactional
	@Modifying
	@Query("delete from Articulo a where a.id=?1")
	public void eliminar(Integer id);
	
}
