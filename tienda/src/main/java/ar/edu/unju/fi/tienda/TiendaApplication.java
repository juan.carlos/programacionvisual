package ar.edu.unju.fi.tienda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * 
 * @author jzapana
 *
 */
@SpringBootApplication
public class TiendaApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(TiendaApplication.class, args);
	}

}
