package ar.edu.unju.fi.tienda.service;

import java.util.List;

import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tienda.domain.Articulo;
import ar.edu.unju.fi.tienda.domain.Pais;
import ar.edu.unju.fi.tienda.repository.ArticuloRepository;
import ar.edu.unju.fi.tienda.repository.PaisRepository;

@Service
public class ArticuloService {

	private final ArticuloRepository articuloRepository;
	private final PaisRepository paisRepository;
	
	
	public ArticuloService(ArticuloRepository articuloRepository, PaisRepository paisRepository) {
		this.articuloRepository = articuloRepository;
		this.paisRepository = paisRepository;
	}

	
	public List<Articulo> buscarDestacados() {
		return articuloRepository.buscarTodos();
	}
	
	public List<Articulo> buscar(String nombre) {
		return articuloRepository.findByNombreContaining(nombre);
	}


	public void guardar(Articulo articulo) {
		articuloRepository.save(articulo);		
	}
	
	public List<Pais> getPaises(){
		return paisRepository.findAll();
	}


	public Articulo getBy(Integer id) {
		return articuloRepository.getOne(id);
	}


	public void eliminar(Articulo articulo) {
		//articuloRepository.delete(articulo);		
		articuloRepository.eliminar(articulo.getId());
	}	
}
