package ar.edu.unju.fi.tienda.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.unju.fi.tienda.domain.Pais;

public interface PaisRepository extends JpaRepository<Pais, Integer> {

}
