package ar.edu.unju.fi.tienda.controller;

import java.util.List;

import org.jfree.chart.servlet.DisplayChart;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import ar.edu.unju.fi.tienda.domain.Articulo;
import ar.edu.unju.fi.tienda.domain.Pais;
import ar.edu.unju.fi.tienda.service.ArticuloService;
/**
 * Listado de instrumentos
 * @author jzapana
 *
 */
@Controller
public class ListadoController {
	
	private final ArticuloService articuloService;
	
	public ListadoController(ArticuloService articuloService) {
		 this.articuloService = articuloService;
	}
	
	/**
	 * 
	 * @param model permite agregar objetos para que sean usados en la vista
	 * @return
	 */
	@RequestMapping("/")
	public String ListarArticulos(Model model) {
		List<Articulo>articulos =articuloService.buscarDestacados();
		model.addAttribute("articulos", articulos);
		return "listaArticulos";
	}
	
	@RequestMapping("/buscar")
	public String buscar(String nombre, Model model) {
		List<Articulo>articulos =articuloService.buscar(nombre);
		model.addAttribute("articulos", articulos);
		return "listaArticulos";
		
	}

	@RequestMapping("/cargar")
	public String cargar(Model model) {
		Articulo articulo = new Articulo();
		List<Pais> paises = articuloService.getPaises();
		model.addAttribute("articulo", articulo);
		model.addAttribute("paises", paises);
		return "cargarArticulo";
	}
	
	@RequestMapping("/guardar")
	public String guardar(Model model, @ModelAttribute("articulo") Articulo articulo ) {
		articuloService.guardar(articulo);
		model.addAttribute("mensaje", "El Articulo de Registro Correctamente");
		return "cargarArticulo";
	}
	
	@RequestMapping("/verDetalle/{id}")
	public String verDetalle(Model model, @PathVariable("id") Integer id ) {
		Articulo articulo =  articuloService.getBy(id);
		List<Pais> paises = articuloService.getPaises();
		model.addAttribute("articulo", articulo);
		model.addAttribute("paises", paises);
		return "cargarArticulo";
	}
	
	@RequestMapping("/eliminar/{id}")
	public String eliminar(Model model, @PathVariable("id") Integer id ) {
		Articulo articulo =  articuloService.getBy(id);
		articuloService.eliminar(articulo);
		return "redirect:/";
	}
	
//	@RequestMapping("/eliminarPais/{id}")
//	public String eliminarPais(Model model, @PathVariable("id") Integer id ) {
//		Pais pais =  paisService.getBy(id);
//		List<Articulo> articulos = articuloService.getBy(pais);
//		if (articulos.size() > 0) {
//			model.addAttribute("error", "El Pais no se Puede Eliminar");
//			
//		}else{
//			paisService.eliminar(pais);			
//		}
//		return "redirect:/";		
//	}
	
	@Bean
	public ServletRegistrationBean MyServlet() {
		return new ServletRegistrationBean<>(new DisplayChart(),"/chart");
	}
	
	@RequestMapping("/testjfreechart")
	public String jfreechart() {
		return "jfreechart";
	}
	
}
